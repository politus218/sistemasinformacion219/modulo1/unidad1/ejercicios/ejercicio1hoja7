﻿--Ejercicio 2 Hoja 8.

DROP DATABASE b20190614;
CREATE DATABASE IF NOT EXISTS b20190614;
USE b20190614

CREATE TABLE zona_urbana(
  nombrezona varchar (100),
  categoria varchar (100),
  PRIMARY KEY (nombrezona)
  );

CREATE TABLE bloque_casas (
  calledir varchar (100),
  numerodir varchar (100),
  npisos varchar (100),
  PRIMARY KEY (calledir,numerodir) 
  );

CREATE TABLE casa_particular (
  numeroc int,
  nombrezona varchar (100),
  m2 int,
  PRIMARY KEY (numeroc,nombrezona),
  CONSTRAINT fkcasaparticular FOREIGN KEY (nombrezona) REFERENCES zona_urbana (nombrezona)
  );

CREATE TABLE piso (
  planta varchar (10),
  puerta varchar (10),
  calledir varchar (10),
  numerodir varchar (10),
  m2 int,
  PRIMARY KEY (planta,puerta,calledir,numerodir),
  CONSTRAINT fkpisocalledirdebloque FOREIGN KEY (calledir,numerodir) REFERENCES bloque_casas (calledir,numerodir)
  );

CREATE TABLE persona ( 
  dni int (9),
  nombre varchar (100),
  edad int,
  numeroc int,
  nombrezona varchar(100),
  planta varchar (10),
  puerta varchar (10),
  calledir varchar (10),
  numerodir varchar (10),
  PRIMARY KEY (dni),
  CONSTRAINT fkpersonaennombrezona FOREIGN KEY (numeroc,nombrezona) REFERENCES casa_particular (numeroc,nombrezona),
  CONSTRAINT fkpersonaenplanta FOREIGN KEY (planta,puerta,calledir,numerodir) REFERENCES piso (planta,puerta,calledir,numerodir)   
  );

CREATE TABLE poseec ( 
  numeroc int,
  nombretorre varchar (100),
  dni int (9),
  PRIMARY KEY (numeroc,nombretorre,dni),
  CONSTRAINT fkposeecasaparticular FOREIGN KEY (numeroc,nombretorre) REFERENCES casa_particular(numeroc,nombrezona),
  CONSTRAINT fkposeecasa FOREIGN KEY (dni) REFERENCES persona (dni)
  );

CREATE TABLE poseep (
  planta varchar (10),
  puerta varchar (10),
  calledir varchar (100),
  numerodir varchar (10),
  dni int (9),
  PRIMARY KEY (planta, puerta, calledir, numerodir, dni),
  CONSTRAINT fkposeepiso FOREIGN KEY (planta,puerta,calledir,numerodir) REFERENCES piso (planta,puerta,calledir,numerodir),  
  CONSTRAINT fkposeepisopersona FOREIGN KEY (dni) REFERENCES persona (dni)
  );